#include <iostream>
using namespace std;

void input(string opt)
{
    //Code
    int num1, num2, res;
    cout << "Enter 1st number: ";
    cin >> num1;

    cout << "Enter 2nd number: ";
    cin >> num2;

    if (opt == "+")
    {
        res = num1 + num2;
        cout << "Sum of " << num1 << " and " << num2 << " is " << res << endl;
    }
    else if (opt == "-")
    {
        res = num1 - num2;
        cout << "Substraction of " << num1 << " and " << num2 << " is " << res << endl;
    }
    else if (opt == "x")
    {
        res = num1 * num2;
        cout << "Multiplication of " << num1 << " and " << num2 << " is " << res << endl;
    }
    else if (opt == "/")
    {
        res = num1 / num2;
        cout << "Division of " << num1 << " and " << num2 << " is " << res << endl;
    }
}

int main()
{
    //Code
    string opt, string;
    string = "You choose ";

    while (true)
    {
        cout << "Enter operator: ";
        cin >> opt;

        if (opt == "+")
        {
            cout << string << "Addition\n";
            input(opt);
        }
        else if (opt == "-")
        {
            cout << string << "Substraction\n";
            input(opt);
        }
        else if (opt == "x")
        {
            cout << string << "Multiplication\n";
            input(opt);
        }
        else if (opt == "/")
        {
            cout << string << "Division\n";
            input(opt);
        }
        else
        {
            cout << "Operation not avialable!\n";
            break;
        }
    }
}
